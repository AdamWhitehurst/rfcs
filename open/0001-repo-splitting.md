* Feature Name: Repository Split
* Start Date: 2018-07-15
* RFC PR:
* Rust Issue:

## Summary
We should split the repository into multiple individual crates.

## Motivation
Given how Rust's crate system works, if we were to publish each of our crates on crates.io we would not need to manage git submodules, so the project would not be that hard to manage and our main repository can be focused on the game. Splitting most of the crates into individual repositories may make it easier to develop the game, since game development will be focused on the main repo and the other crates will be open source projects of their own.

## Guide-level Explanation
We should take the crates that are currently in the `game` repository, and move them into an `easy-p2p`, `voxygen`, `game`, and `ui` repository.
## Reference-level Explanation
I propose we factor out the crates as follows:
```
veloren/easy-p2p
|> src/
  |> client/
  |> server/
  |> lib.rs
```
Here we combine the `client` and `server` crates into one networking crate that handles client-server P2P connection. Having two separate crates for the `client` and `server` side of a P2P connection seems like a hassle, and since they work with each other I think it would make sense to put them together. Refactoring the API could be as simple as moving the current `client` and `server` APIs into `easy-p2p::Client` and `easy-p2p::Server`, respectively.  
The name is not even close to final, it's simply a placeholder.
```
veloren/voxygen
```
This is simply the voxel rendering engine moved to a new repo. However, we should probably refactor this crate heavily to only handle rendering, instead of handling input like it currently does.
```
veloren/game
|> src/
  |> common/
  |> misc/
  |> headless/
  |> input/
  |> region/
  |> server-cli/
  |> world/
  |> main.rs
```
This is the main game repo. The `common` crate has some features that we may want to use in the p2p crate, but otherwise would stay. The `misc/` folder, `headless`, `region`, `server-cli`, and `world` are all core parts of Veloren, and while world generation may fit as another crate, I don't think it should be due to its tight integration into the game.  
The `input` folder would be a new crate for input handling. While I didn't list it in the tree, I think in the future we should add a `frontend` folder where we abstract away the frontend so players will be able to drag and drop `.exe`s and change a setting to play under a different frontend. This is something we should consider for the future, but is too complex to implement now IMO.
```
veloren/ui
```
This is a new repository that would hold the `ui` crate we intend to create.

## Drawbacks
It may make it harder to manage development of all aspects of the project.

## Rationale and Alternatives
There are a couple of notable benefits to splitting the repository:
- Forces compartmentalization, which makes the project cleaner and more modular
- Enables other developers to easily use each of our crates in their own project
While it may be harder to manage all aspects of the project, the split may actually make it easier if managed well. It would scale better with a larger group of programmers, and we can focus on per-crate milestones instead of building everything at the same time. This can speed along development and make development much cleaner.

## Prior Art
Rust is heavily focused on the concept of crates, and due to the ease of use of `cargo` and the fact that all of the folders in the main repo are crates already, it only makes sense to separate them completely from the main Veloren repository.

## Unresolved Questions
This would make planning and milestones a lot easier and clearer to outside developers/users.