- Feature Name: Guide for detailing infrastructure management and maintenance.
- Start Date: 2019-09-15
- RFC Number:
- Tracking Issue:

# Summary
[summary]: #summary

This is a document detailing how the official infrastructure of the veloren project should be altered and maintained.
It contains best practices along with guides and what do to and what not to ensure the stability and security of the infrastructure.

# Motivation
[motivation]: #motivation

This guide needs to exist to ensure that we minimize mistakes and errors while maintaining out infrastructure as well as altering it.
We also need to have people responsible for making sure this happens and that regular system maintenance is performed on all hosts managed by us.

# Usage
[usage]: #usage

Members of the infrastructure admin team should refer to this guide at any point that they are not sure what to do.

# Guide
[guide]: #guide

## Terminology

Any use of the word `server` in this guide refers to a software component like the authentication server.
With the word `host` I refer to the machines that the software runs on.

## Diagram

Services marked as green can scale horizontally and are relatively easy to do so.
Reds are services that scale worse and need good hardware.

![diagram](https://i.imgur.com/a87sXB7.png "diagram")

## Hoster company

After a significant amount of discussion in the Veloren Discord server a lot of us have come to the conclusion that Hetzner is our best option for hosting our servers.
Hetzner was chosen due to having teams integration for shared management and having good prices compared to other options we looked at (DO, Packet, Netcup).

# Note about things that are not hosted on Hetzner

Currently we have things like gameservers that are not hosted on Hetzer. A list of these and
who is responsible for that specific service should be kept in a document in a new repo at `veloren/doc` or similiar.
This includes but is not limited to:

- CI runners
- Gameservers
- Bots of all kinds

## Host provisioning

Before creating a host it should be heavily discussing with core developes and or admins to ensure a valid use-case.
Hosts should be created with the `Debian 10` image as it is the operating system we should use due to its security and stability.
Using Hetzners key feature we can deploy all registered keys to a host at creation automatically.

### TO-DO list after host creation

- Configure the SSH server to listen on a nonstandard port. (15422)
- Run updates `apt-get update && apt-get upgrade`.
- Install utility and administration packages: htop, nload, iotop, netcat
- Install the needed software and dependencies if any to perform the task it was provisioned to perform.
- Configure automatic updates using unattended-upgrades.
- Configure hourly backups (see backups section).

## Regular maintenance

At least once every month, someone should SSH into each host and perform the following tasks.

- Check for failed SystemD services and take appropriate action.
- Check system logs for errors.
- Check application logs for errors.

## Security

Only a carefully selected list should have administrative access to this infrastructure.
Administrative access consists of having access the project in the Hetzner Cloud panel as well as having the public portion of their SSH key on the hosts.

Each hosts should have two primary accounts. Root and veloren
veloren is a regular user with SUDO access that every person on the admin list has an SSH keypair for.
It is the user that is logged into when a task needs to be performed.

Each user on the admin list should have an unique keypair of RSA4096 public and private keys. The veloren user should have the public keys for each of these.
These public keys are managed on the Hetzner Cloud panel using the Keys feature.

If someone on that list suddenly disappears their public keys should be removed everywhere, removing access.

## Initial team

The initial team I suggest that has panel and SSH access consists of the following

- Zesterer
- xMAC94x
- Acrimon (xacrimon)
- Songtronix

## Planned infrastructure

This is a list of all currently planned infrastructure at the time of writing this RFC.

Authentication and account management (Acrimon) & Airshipper (Songtronix):

- 1x CX11 Virtual Host

## Getting something hosted officially

If you have something very useful to Veloren that you believe should be hosted on official infrastructure speak with the current infrastructure team
on Discord, Gitlab or via email.

## Joining the infrastructure admin team

To be given panel access speak with Acrimon via discord or shoot an email to `joel.wejdenstalgmail.com` with the same email address as listed on Gitlab.
There should also be very good reason for joining the team and it shall be discussed with the Core Developers.

When you join the team and is given panel access you should create a RSA4096 keypair and upload the public key via Hetzners Keys feature.
You should then notify the team to have your public key added to hosts. The initial team should all do this.

## Backups

As per good practice backups will be taken hourly using `restic` (shrug gophers). For storing data I suggest an atleast temporary solution using a bucket on Wasabi
as a data target with the S3 protocol. I propose this due to me having significant free space left over there.

To set up this there are a few things that need to be done. First, the `restic` package should be installed.
Then a set of configuration files should be creating in `/opt/restic` with an S3 access key, a S3 secret key and a repository password
(ask Acrimon about these). Then a shell script should be used that sources these files and runs restic.

These a list of files to be placed there and their contents.

`pass`: A password file containing the password for decrypting the password repository.

`exclude.txt`: A file containing filesystem locations to exclude.

```
/dev/*
/proc/*
/lost+found/*
/mnt/*
/sys/*
/tmp/*
/root/.cache
```

`config.sh`: Contains the S3 authentication keys and the bucket name

```
#!/bin/bash

export RESTIC_REPOSITORY=""
export RESTIC_PASSWORD_FILE="/opt/restic/pass"
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""
```

`backup.sh`: The actual backup script

```
#!/bin/bash

tag1="machinename"
tag2="rootdir"

source /opt/restic/config.sh

restic backup -o s3.connections="32" --tag $tag1 --tag $tag2 --exclude-file=exclude.txt /
```

This script should be modified to additionally create backups for DB exports for example. This can be done by creating a backup using the `stdin` feature of restic
and by making a sqldump of the databases.

This should be configured per host as needed.

A cronjob should be set up for root that runs this script hourly.

This is a backup system that has been serving me very well for multiple years without error.

## Funding

Until we have a solid strategy for this I am willing to bankroll this since the cost isn't huge and I can afford it without issue.
Additionally it will take 3 months of nonpayment before Hetzner cancels any hosts so we should be safe.

## Other

Before any drastic change is made the rest of the team should be consulted to ensure agreement.

# Unresolved questions
[unresolved]: #unresolved-questions