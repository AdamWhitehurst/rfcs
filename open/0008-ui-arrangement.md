- Feature Name: ui-corner-focus
- Start Date: 2018-07-16
- Tracking Issue: 0008

# Summary
[summary]: A method to place UI elements more efficiently

# Motivation
[motivation]: Having unobstructed view and easier control of the UI

# Guide-level explanation
[guide-level-explanation]: Buttons like accept, cancel, etc. go at the corners of the screen, preferably spread along the bottom.
Pop-up like UI appears as far from the center as possible when in-game (eg. inventory appears huging a border of the screen), buttons on corners and spread along the borders of the screen will make clicking them easier and often without even looking
This changes should make the UI more bearable and easy to use

# Reference-level explanation
[reference-level-explanation]: The feature would be implemented along with the rest of the UI design

# Drawbacks
[drawbacks]: This could make the UI look ugly and similar to mobile games (not degrading mobile games, but their designs tend to be made around the limitations of the device). Also could leave a lot of "weird" and unused space on-screen

# Rationale and alternatives
[alternatives]: Alternatively we could go for the usual buttons offset from the borders wich are slightly closer to the center and may take less time to move the cursor to.

# Prior art
[prior-art]: This feature is commonly used in simple and mobile games to make it as easy as possible to press the buttons and overcome the limitations and inaccuracy of a touch-screen.
This tends to work flawlessly for said purpose since it also frees up space on screen for extra visuals on the small resolution

# Unresolved questions
[unresolved]: The main issues are, ¿how can this be achieved without reducing the visual quality of menus and editing the UI to fit other buttons that could be added in the future but said problem also appears with the common offset buttons and interfaces
